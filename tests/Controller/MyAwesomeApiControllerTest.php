<?php

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MyAwesomeApiControllerTest extends WebTestCase
{
    public function testRootApi() {
        $client = static::createClient();
        $client->request('GET', '/api');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
    }

}