#!/bin/bash


ONDREJ_PPA=ondrej-ubuntu-php-bionic.list

if [ ! -f ${ONDREJ_PPA} ]; then
        LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php
fi

apt-get update && apt-get install -y php7.4-fpm php7.4-curl php7.4-intl php7.4-mbstring \
        php7.4-opcache php7.4-zip php7.4-xml unzip php7.4-cli \
        nginx ruby wget python python-pip


COMPOSER_PATH=/usr/local/bin/composer

if [ ! -f ${COMPOSER_PATH} ]; then
        php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
        php composer-setup.php
        mv composer.phar ${COMPOSER_PATH}
        chmod +x ${COMPOSER_PATH}
fi

systemctl enable nginx
systemctl enable php7.4-fpm

tee /etc/nginx/sites-available/default << END
server {
        listen 80 default_server;
        listen [::]:80 default_server;

        root /var/www/html/my_symfony_project/public;

        location / {
                # try to serve file directly, fallback to index.php
                try_files \$uri /index.php\$is_args\$args;
        }

        # optionally disable falling back to PHP script for the asset directories;
        # nginx will return a 404 error when files are not found instead of passing the
        # request to Symfony (improves performance but Symfony's 404 page is not displayed)
        # location /bundles {
        #     try_files \$uri =404;
        # }

        location ~ ^/index\.php(/|$) {
            fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
            fastcgi_split_path_info ^(.+\.php)(/.*)$;
            include fastcgi_params;

            # optionally set the value of the environment variables used in the application
            # fastcgi_param APP_ENV prod;
            # fastcgi_param APP_SECRET <app-secret-id>;
            # fastcgi_param DATABASE_URL "mysql://db_user:db_pass@host:3306/db_name";

            # When you are using symlinks to link the document root to the
            # current version of your application, you should pass the real
            # application path instead of the path to the symlink to PHP
            # FPM.
            # Otherwise, PHP's OPcache may not properly detect changes to
            # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
            # for more information).
            fastcgi_param SCRIPT_FILENAME \$realpath_root\$fastcgi_script_name;
            fastcgi_param DOCUMENT_ROOT \$realpath_root;
            # Prevents URIs that include the front controller. This will 404:
            # http://domain.tld/index.php/some-path
            # Remove the internal directive to allow URIs like this
            internal;
        }

        # return 404 for all other php files not matching the front controller
        # this prevents access to other php files you don't want to be accessible.
        location ~ \.php$ {
            return 404;
        }

        error_log /var/log/nginx/symfony.log;
        access_log /var/log/nginx/symfony.log;

}
END


systemctl restart nginx
systemctl restart php7.4-fpm


wget https://aws-codedeploy-eu-west-1.s3.eu-west-1.amazonaws.com/latest/install
chmod +x install
./install auto

systemctl start codedeploy-agent

mkdir /etc/awslogs/

echo "
[general]
state_file = /var/awslogs/state/agent-state

[/var/log/cloud-init-output.log]
file = /var/log/cloud-init-output.log
buffer_duration = 5000
log_group_name = codedeploy-poc-var-log-cloud-init-output
log_stream_name = {instance_id}
datetime_format = %b %d %H:%M:%S
initial_position = start_of_file

[/var/log/aws/codedeploy-agent]
file = /var/log/aws/codedeploy-agent/codedeploy-agent.log
buffer_duration = 5000
log_group_name = codedeploy-poc-codedeploy-agent
log_stream_name = {instance_id}
datetime_format = %b %d %H:%M:%S
initial_position = start_of_file


[/var/log/nginx/symfony.log]
file = /var/log/nginx/symfony.log
buffer_duration = 5000
log_group_name = codedeploy-poc-symfony
log_stream_name = {instance_id}
datetime_format = %b %d %H:%M:%S
initial_position = start_of_file

" > /etc/awslogs/awslogs.conf

wget https://s3.amazonaws.com/aws-cloudwatch/downloads/latest/awslogs-agent-setup.py
python ./awslogs-agent-setup.py --region eu-west-1 -c /etc/awslogs/awslogs.conf -n



