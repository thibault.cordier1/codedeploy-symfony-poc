<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class MyAwesomeApiController extends AbstractController
{
    /**
     * @Route("/api", name="my_awesome_api")
     */
    public function index()
    {
        return new JsonResponse(array(
            'version' => "1"
        ), 200);
    }
}
